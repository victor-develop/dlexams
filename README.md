# Turn a form to google response

```js
var inps = document.querySelectorAll('input')
var list = []
for(let i = 0; i<inps.length;i++) list.push(inps[i])
var goodInps = list.filter(n => n.classList.length != 0)
let final = goodInps.map(n => ({label: n.getAttribute('aria-label').replace(/_ID_.*/,''), name: n.name}))
// now copy this
JSON.stringify(final)
// and this
$('form').action
```

# WARN:
Whenever you change something:
 - comment translateGoogleForm
 - re-create the form
 - change the response link
 - change const mapping in translateGoogleForm
 - uncomment translateGoogleForm