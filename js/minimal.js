(
  function(window) {
    const meta= window.meta || {part1:{title:"Background Information",questions:{ename:{desc:"Your English name: (can be a made-up name)"},gender:{desc:"Gender",option:["Female","Male"]},age:{desc:"Age",option:["20-30","31-40","41-50"]},job:{desc:"Job/major: working as a: (fill 'None' if not applicable)"},studyMajor:{desc:"You are studying: (fill 'None' if not applicable)"},lang:{desc:"Mother language/Native language",option:["Cantonese","English"]},from:{desc:"You are from:",option:["Hong Kong","Macau","Australia","Other"]},yrsLearn:{desc:"How many years have you been learning English:",type:"number"},yrsStayEngCountry:{desc:"How many years did you ever live in an English-speaking country?(Put a number, can be 0)"}}},part2:{title:"Comprehension Test",fillblanks:[{title:"Mars",paragraph:"According to scientists Mars is the best bet for a home for aliens. This is because Mars has got water on it, and there has to be water on a planet to support life. However th{q1} are tho{q2} to b{q3} thousands o{q4} solar sys{q5} in sp{q6} - planets go{q7} around a cen{q8} star li{q9} the s{q10} . However, w{q11} have on {q12} found o{q13} other i{q14} our so{q15} system. I{q16} is poss{q17} that li{q18} may ex{q19} in th{q20} solar sys{q21} but on{q22} if t{q23} conditions a{q24} right. Exp{q25} also think that aliens will not live outside. Many environments are too harsh for life as we know it, so space missions will be looking underground.\n"},{title:"Queen Mary 2",paragraph:'Children are among 13 people who have been killed in an accident on new British cruise ship Queen Mary 2 in France after a gangway collapsed. A spok{q1} from t{q2} General Hosp{q3} in Saint Nazaire wh{q4}the inj{q5} were ta{q6} confirmed th{q7} " two chil{q8} died a{q9} the sc{q10} ". Another 32 peo{q11} thought t{q12} be rela{q13} of peo{q14} working o{q15} the sh{q16} -were inj{q17} in t{q18} accident. T{q19} ship i{q20} not fini{q21} yet. A spok{q22} for t{q23} shipyard ow{q24} said th{q25} 12 people were dead, 12 were badly injured and another 20 were lightly injured. The Queen Mary 2 is the largest passenger ship ever made and is as tall as a 23 floor building, it has cost around £550m to build. Her first voyage is expected to take place in December, when she sails to her home port of Southampton.\n'},{title:"On-line dating",paragraph:"Candlelight dinners and lunch in the park will likely never lose their appeal with romance seekers, but today's singles want more than that. Many peo{q1} are qu{q2} specific ab{q3} what th{q4} want fr{q5}  their roma{q6} partners, a{q7} rising expect{q8} for mee{q9} soul ma{q10}, social bud{q11}, casual encou{q12}, and li{q13} long relati{q14} may co{q15} in pa{q16}  from t{q17}  advancements i{q18} online dat{q19}. You n{q20}  longer ha{q21}to set{q22} on dat{q23} the b{q24} next do{q25}, although he may still be your best friend. You can afford to be selective. Thanks to on- line dating, the capability now exists to meet potential partners across the globe or the other side of town, simply by logging in.\n"},{title:"Stress",paragraph:"Stress in itself is not necessarily harmful. We ne{q1} goals a{q2} challenges i{q3}  life, o{q4} we g{q5}  bored. So{q6} people c{q7}  tolerate a{q8} sorts o{q9} major li{q10} changes wit{q11} feeling pres{12}, while oth{q13} find i{q14} difficult t{q15} cope wh{q16} life ge{q17} stressful. How{q18}, we d{q19} know th{q20} too mu{q21}is dama{q22}. Stress i{q23} a we{q24} - known trigger for depression and can also affect your physical health- so it is important to identify the stress factors in your life and do everything you can to minimise them.\n"}]},part3:{title:"Email Writing",stepQuestions:{roomate:[{name:"exchange-roomate",reading:"A君曾經是你大學時候的一位交換生（只會說英文），你們曾經是一年的室友，成為了好朋友。但是你們已經有兩年沒有見面了。最近你準備到A君的國家旅行，想知道是否能在他/她家借宿幾天，以及讓他/她給你推薦一些在當地的美景美食等等。\n",postRead:"Please write an email to your friend according to the situation described above.",next:"exchange-roomate-reply"},{name:"exchange-roomate-reply",hide:!0,preRead:"The following are the replies you received from your friend regarding your requests. Please write an email back to them respectively.",reading:"Reply from your friend\n\nHey friend,\nIt’s been so long! Nice to hear from you! I can’t believe that you’re coming to visit. I’m so excited! Of course I’ll be happy to host you. You can stay with me while you are here, and I’ll definitely bring you to some great food and places. Just let me know when you’re coming so I can make some plans 😊.\nCheers,\nA\n",lastStep:!0}],"apply-leave":[{name:"apply-leave",reading:"你在一家外企小公司剛工作了半年。由於私人原因你想要向公司請十天帶薪假，但通常工作满一年才能申请年假。你的直屬上司告訴你這種情況他沒有辦法批假，你需要直接向公司副總裁(C. Smith)申請。由於剛到公司半年，你和這位副總還沒有見過面，於是你決定通過郵件向副總提出申請十天休假。\n",postRead:"Please write an email to your boss C. Smith according to the situation described above.",next:"apply-leave-reply"},{name:"apply-leave-reply",hide:!0,reading:"Reply from your boss\nDear XX (your name),\nI am sorry for the disappointing news, but please understand that you are not yet entitled to take the annual leave as you have not met the company’s requirement – all employees must work for the company for at least a year to take the annual time off.  Should you have other question, please refer to the company policy.\nSincerely,\nC. Smith\nVice President \n",preRead:"The following are the replies you received from your friend and your boss regarding your requests. Please write an email back to them respectively.",lastStep:!0,closeAll:!0}]}}};
    function addTextInput(partName, qName, question) {
      const id = partName+'_'+qName
      const html = `
        <label for="${id}">${question.desc}</label>
        <input required id="${id}" name="${id}" ${question.type === 'number'?'pattern="[0-9]+"':''} />
      `
      return html
    }
    function addSelectInput(partName, qName, question) {
      const id = partName+'_'+qName
      const html = `
        <label for="${id}">${question.desc}</label>
        <select required id="${id}" name="${id}">
            <option value="">Click To Select</option>
          ${question.option.map(op => (`
            <option value="${op}">${op}</option>
          `))}
        </select>
      `
      return html
    }
    function mapInputType(part, inp, metaData) {
      const question = metaData[part].questions[inp]
      if (!question.option) {
        return addTextInput(part, inp, question)
      } else {
        return addSelectInput(part, inp, question)
      }
    }
    function turnQuestion(part, metaData) {
      const inputs = Object.keys(metaData[part].questions)
        .map(inp => mapInputType(part, inp, metaData))
        .join('<br>');
      const html = `
      <h2>${metaData[part].title}</h2>
      <p>
        ${inputs}
      </p>
      `
      return html;
    }
    function turnFillBlanks(part, metaData) {
      function createBlank(pre, bname, post) {
        return `      
        <span class="p-inp-box">${pre}
          <input required id="${part}_${bname}" name="${part}_${bname}" type="text">
          ${post}
        </span>`  
      }
      function mapFillBlank(entry) {
        const tokens = entry.paragraph
          .trim().split(/\s/)

        const parsedChunks = {}
        tokens.map(t => t.match(/(\S*){(.*?)}(\S*)/)).filter(x => x !== null)
        .forEach(matched => {
          parsedChunks[matched[0]] = createBlank(matched[1], `${entry.title.replace(/\s+/g, '')}_${matched[2]}`, matched[3])
        })

        const html = `
          <p>Text</p>
          <h3>${entry.title}</h3>
          <p>
            ${tokens.map(x => (parsedChunks[x] === undefined
                ? x
                : parsedChunks[x]
              ))
              .join(' ')
            }
          </p>
        `
        return html;
      }
      const inputs = metaData[part].fillblanks.map(mapFillBlank).join('')
      return `
        <h2>${metaData[part].title}</h2>
        <p>${outputNote(metaData[part])}</p>
        ${inputs}
      `
    }

    function outputNote(obj) {
      if (obj.note)
        return `
          <strong>${obj.note}</strong>
        `
      return ''
    }

    function turnStepQuestions(part, metaData) {
      function turnSection(secName) {
        return `<strong>Situation</strong> <br>` + metaData[part][stepQuestions][secName]
        .map(ques => {
          const inputName = `${part}_${ques.name.replace('-','_')}`
          const output = `
            <div class="${ques.name} ${ques.hide === true?'hide':''}">
                
                ${ques.preRead === undefined
                  ? ''
                  : '<p><strong>'+ques.preRead+'</strong></p>'
                }
                
                ${ques.reading === undefined
                  ? ''
                : '<p>'+ques.reading.replace(/(?:\r\n|\r|\n)/g, '<br>')+'</p>'
                }
                ${ques.postRead === undefined
                  ? ''
                  : '<p><strong>'+ques.postRead+'</strong></p>'
                }
              
              <p>
                <textarea class="${inputName}" name="${inputName}" rows="6" required></textarea>
                <input 
                  type="button" value="Send"
                  data-letter-name="${inputName}"
                  ${ques.next === undefined
                    ? ''
                    :  `data-next-letter-name="${ques.next}"`
                  }
                  ${ques.closeAll === true
                    ? `data-show-submit="true"`
                    : ''
                  }
                  onclick="finishLetter(this)" />
              </p>
            </div>
          `
          return output
        })
        .join('')
      }
      const stepQuestions = 'stepQuestions'
      const sectionNames = Object.keys(metaData[part][stepQuestions])
      const sections = sectionNames.map(turnSection).join('')
      const html = `
        <h2>${metaData[part].title}</h2>
        <p> <strong>Writing emails according to given situations. Click "Send" after you complete each email.</strong> </p>
        ${location.pathname.includes('ncs')?`<p> <strong>根據情景寫郵件. 當你寫完每一封郵件請點擊 "Send"。 </strong> </p>`:''}
        ${sections}
      `
      return html
    }

    function translate2GoogleForm() {
      const mappings = [{"label":"part1_studyMajor","name":"entry.435565073"},{"label":"part1_yrsLearn","name":"entry.1300680142"},{"label":"part1_job","name":"entry.1312733074"},{"label":"part2_TitanicSank_q91","name":"entry.334558618"},{"label":"part2_Health_q6","name":"entry.1133346372"},{"label":"part2_Health_q7","name":"entry.1867362416"},{"label":"part2_Health_q8","name":"entry.502515900"},{"label":"part2_Health_q9","name":"entry.1475892696"},{"label":"part2_Health_q2","name":"entry.588211694"},{"label":"part2_Health_q3","name":"entry.1202134882"},{"label":"part2_Health_q4","name":"entry.1749895496"},{"label":"part1_from","name":"entry.797661827"},{"label":"part2_Health_q5","name":"entry.1813846629"},{"label":"part3_apply_leave","name":"entry.2002423568"},{"label":"part2_Health_q1","name":"entry.1951613863"},{"label":"part2_TitanicSank_q18","name":"entry.2111948606"},{"label":"part2_TitanicSank_q19","name":"entry.924277779"},{"label":"part2_TitanicSank_q16","name":"entry.2141981735"},{"label":"part2_TitanicSank_q17","name":"entry.50349130"},{"label":"part2_Health_q15","name":"entry.2124919373"},{"label":"part2_TitanicSank_q10","name":"entry.1187955229"},{"label":"part2_Health_q16","name":"entry.921771575"},{"label":"part2_TitanicSank_q11","name":"entry.198956865"},{"label":"part2_Health_q13","name":"entry.549233765"},{"label":"part2_Health_q14","name":"entry.430053478"},{"label":"part2_Health_q19","name":"entry.1509609371"},{"label":"part2_TitanicSank_q14","name":"entry.2027816104"},{"label":"part2_TitanicSank_q15","name":"entry.165553311"},{"label":"part2_Health_q17","name":"entry.928418498"},{"label":"part2_TitanicSank_q12","name":"entry.2117663813"},{"label":"part2_Health_q18","name":"entry.703648873"},{"label":"part2_TitanicSank_q13","name":"entry.907336286"},{"label":"part2_MyAmbition_q19","name":"entry.545606306"},{"label":"part2_MyAmbition_q18","name":"entry.494702526"},{"label":"part2_MyAmbition_q17","name":"entry.1817831778"},{"label":"part2_Health_q11","name":"entry.1766862995"},{"label":"part2_MyAmbition_q16","name":"entry.1232639664"},{"label":"part2_Health_q12","name":"entry.1793829181"},{"label":"part2_MyAmbition_q15","name":"entry.1308979878"},{"label":"part2_MyAmbition_q14","name":"entry.753947529"},{"label":"part2_Health_q10","name":"entry.1514333344"},{"label":"part2_MyAmbition_q13","name":"entry.1368108380"},{"label":"part2_MyAmbition_q12","name":"entry.75393557"},{"label":"part2_MyAmbition_q11","name":"entry.1140244601"},{"label":"part2_MyAmbition_q10","name":"entry.2033772296"},{"label":"part2_TitanicSank_q29","name":"entry.1629929456"},{"label":"part2_TitanicSank_q27","name":"entry.1984875041"},{"label":"part2_TitanicSank_q28","name":"entry.593791439"},{"label":"part1_age","name":"entry.280984070"},{"label":"part2_Health_q26","name":"entry.511417406"},{"label":"part2_TitanicSank_q21","name":"entry.1553132622"},{"label":"part2_Health_q27","name":"entry.1478185476"},{"label":"part2_TitanicSank_q22","name":"entry.1289502865"},{"label":"part2_Health_q24","name":"entry.255654493"},{"label":"part2_Health_q25","name":"entry.1483385764"},{"label":"part2_TitanicSank_q20","name":"entry.641293687"},{"label":"part2_TitanicSank_q25","name":"entry.1262574087"},{"label":"part2_TitanicSank_q26","name":"entry.820716070"},{"label":"part2_Health_q28","name":"entry.472133578"},{"label":"part2_TitanicSank_q23","name":"entry.426344538"},{"label":"part2_Health_q29","name":"entry.1069022420"},{"label":"part2_TitanicSank_q24","name":"entry.202406481"},{"label":"part3_exchange_roomate","name":"entry.1104034309"},{"label":"part2_MyAmbition_q29","name":"entry.1756867376"},{"label":"part2_MyAmbition_q28","name":"entry.1221372746"},{"label":"part2_Health_q22","name":"entry.1883060875"},{"label":"part2_MyAmbition_q27","name":"entry.350194468"},{"label":"part2_Health_q23","name":"entry.352735290"},{"label":"part2_Stress_q11","name":"entry.793828535"},{"label":"part2_MyAmbition_q26","name":"entry.266421509"},{"label":"part2_Health_q20","name":"entry.774735461"},{"label":"part2_Stress_q10","name":"entry.90746585"},{"label":"part2_MyAmbition_q25","name":"entry.263188083"},{"label":"part2_Health_q21","name":"entry.1246810536"},{"label":"part2_MyAmbition_q24","name":"entry.581194809"},{"label":"part2_Stress_q16","name":"entry.230963571"},{"label":"part2_MyAmbition_q23","name":"entry.1462170111"},{"label":"part2_Stress_q15","name":"entry.1695470656"},{"label":"part2_MyAmbition_q22","name":"entry.1566569425"},{"label":"part2_Stress_q14","name":"entry.512304491"},{"label":"part2_MyAmbition_q21","name":"entry.1160026903"},{"label":"part2_Stress_q13","name":"entry.632962083"},{"label":"part1_lang","name":"entry.756730904"},{"label":"part2_MyAmbition_q20","name":"entry.533042889"},{"label":"part2_Stress_q19","name":"entry.2056191164"},{"label":"part2_Stress_q18","name":"entry.1170158094"},{"label":"part2_Stress_q17","name":"entry.212147326"},{"label":"part2_TitanicSank_q30","name":"entry.792697693"},{"label":"part2_Health_q30","name":"entry.383950451"},{"label":"part2_Stress_q23","name":"entry.215581923"},{"label":"part2_Stress_q22","name":"entry.351796953"},{"label":"part2_Stress_q21","name":"entry.899180143"},{"label":"part2_Stress_q20","name":"entry.533777847"},{"label":"part2_Stress_q24","name":"entry.979370274"},{"label":"part2_MyAmbition_q30","name":"entry.569379359"},{"label":"part2_Stress_12","name":"entry.774905455"},{"label":"part1_gender","name":"entry.1047754569"},{"label":"part3_exchange_roomate-reply","name":"entry.591416241"},{"label":"part2_MyAmbition_q4","name":"entry.1392762140"},{"label":"part2_MyAmbition_q3","name":"entry.377191157"},{"label":"part2_MyAmbition_q2","name":"entry.1149500053"},{"label":"part2_MyAmbition_q1","name":"entry.264694775"},{"label":"part2_TitanicSank_q1","name":"entry.1817357420"},{"label":"part2_TitanicSank_q2","name":"entry.1108407854"},{"label":"part2_TitanicSank_q3","name":"entry.1987470886"},{"label":"part2_TitanicSank_q4","name":"entry.1933647916"},{"label":"part2_TitanicSank_q5","name":"entry.1806642631"},{"label":"part2_TitanicSank_q6","name":"entry.280940503"},{"label":"part2_TitanicSank_q8","name":"entry.704726520"},{"label":"part2_TitanicSank_q71","name":"entry.1504183952"},{"label":"part2_MyAmbition_q8","name":"entry.1729775637"},{"label":"part2_MyAmbition_q7","name":"entry.849924539"},{"label":"part2_MyAmbition_q6","name":"entry.553243248"},{"label":"part1_ename","name":"entry.639901538"},{"label":"part2_MyAmbition_q5","name":"entry.1976648987"},{"label":"part2_MyAmbition_q9","name":"entry.1074150631"},{"label":"part2_Stress_q4","name":"entry.2107864401"},{"label":"part1_yrsStayEngCountry","name":"entry.925494998"},{"label":"part2_Stress_q5","name":"entry.1730128894"},{"label":"part2_Stress_q2","name":"entry.1209233405"},{"label":"part2_Stress_q3","name":"entry.2078191796"},{"label":"part2_Stress_q1","name":"entry.1875626269"},{"label":"part3_apply_leave-reply","name":"entry.2048764402"},{"label":"part2_Stress_q8","name":"entry.1812596775"},{"label":"part2_Stress_q9","name":"entry.1498142127"},{"label":"part2_Stress_q6","name":"entry.1409489504"},{"label":"part2_Stress_q7","name":"entry.1846110256"}];
      mappings.forEach(m => {

        const byName = document.querySelector(`[name=${m['label']}]`)
        if (byName) {
          byName.setAttribute('name', m['name'])
        }
        
        const byId = document.querySelector(`[id=${m['label']}]`)
        if (byId) {
          byId.setAttribute('id', m['name'])
        }

        const label = document.querySelector(`[for=${m['label']}]`)
        if(label) {label.setAttribute('for', m['name'])}
      })
    }

    // const chunks = Object.keys(meta).map((p) => turn2html(p, meta))
    const container = document.createElement('div');
    container.innerHTML = `
      ${meta['part1']? turnQuestion('part1', meta): ''}
      ${meta['part2']? turnFillBlanks('part2', meta): ''}
      ${meta['part3']? turnStepQuestions('part3', meta): ''}
    `
    document.querySelector('.form-elements').appendChild(container)
    translate2GoogleForm();
  }
)(window)

function finishLetter(button) {
  const letterName = button.dataset.letterName
  const nextName = button.dataset.nextLetterName
  const showSubmit = button.dataset.showSubmit
  const textBox = document.querySelector(`.${letterName}`)
  if (!textBox.value.trim()) {
    alert("Please do no leave the textbox empty.")
    return
  }
  if (confirm("Are you sure? No more edit after you confirm.")) {
    button.setAttribute('disabled', 'disabled')
    textBox.setAttribute('readonly', 'readonly')
    if (nextName) {
      document.querySelector(`.${nextName}`).classList.remove('hide')
    }
    if (showSubmit) {
      document.querySelector('button[type=submit]').classList.remove('hide')
    }
  }
}