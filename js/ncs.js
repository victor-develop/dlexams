window.meta = {
  "part1": {
    "title": "Background Information",
    "questions": {
      "ename": {
        "desc": "Your English name: (can be a made-up name)"
      },
      "gender": {
        "desc": "Gender",
        "option": [
          "Female",
          "Male"
        ]
      },
      "age": {
        "desc": "Age",
        "option": [
          "20-30",
          "31-40",
          "41-50"
        ]
      },
      "job": {
        "desc": "Job/major: working as a/an: (fill 'None' if not applicable)"
      },
      "studyMajor": {
        "desc": "You are studying (major): (fill 'None' if not applicable)"
      },
      "lang": {
        "desc": "Mother language/Native language",
        "option": [
          "Cantonese",
          "English"
        ]
      },
      "from": {
        "desc": "You are from:",
        "option": [
          "Hong Kong",
          "Macau"
        ]
      }
    }
  },
  "part3": {
    "title": "Email Writing",
    "stepQuestions": {
      "roomate": [
        {
          "name": "exchange-roomate",
          "reading": "你有一個在大學認識的好朋友。你們曾經在大學的時候做了一年的室友，關係很好，他/她目前在澳洲工作，你們已經有兩年沒有見面了。最近你準備要到他/她的城市旅行，想知道是否能在他/她家借宿幾天，同時你希望他/她給你推薦一些在當地的美景美食等等。",
          "postRead": "請根據以上描述寫一封email給你的朋友(你可自訂朋友姓名)).",
          "next": "exchange-roomate-reply"
        },
        {
          "name": "exchange-roomate-reply",
          "hide": true,
          "preRead": "以下是你的朋友給你的回復。請根據信件內容再給他們回復一封郵件。",
          "reading": "Reply from your friend\n\nHey,\n真的好久不見了！太好了，你要過來找我玩？好啊，你來那幾天就住在我家吧，我可以帶你去逛逛還有吃好吃的，我們一定要好好聚一聚。你定好了日期就告訴我，我好開始計劃一下😊。\nCheers,\nA\n",
          "lastStep": true
        }
      ],
      "apply-leave": [
        {
          "name": "apply-leave",
          "reading": "你在一家小公司剛工作了半年，按規定還沒輪到你申請休假。由於私人原因你想要向公司請十天帶薪假（年假），通常你需要工作滿一年才能申請這個年假。你的直屬上司告訴你這種情況他沒有權利批假，你需要直接向公司副總裁王雲申請。由於剛到公司半年，你和這位副總裁還沒有見過面，於是你決定通過郵件向這位上司提出申請十天休假。",
          "postRead": "請根據以上描述寫一封email給你的上司",
          "next": "apply-leave-reply"
        },
        {
          "name": "apply-leave-reply",
          "hide": true,
          "reading": "XX小姐/先生\n你好，根據公司規定，所有工作滿一年或以上的員工可以申請十天或以上的年假。由於你的工作尚未滿一年，所以很抱歉公司無法批准你的休假申請，請諒解。\n王雲\n副總裁\n謹上\n",
          "preRead": "以下是你的上司給你的回復。請根據信件內容再給他們回復一封郵件。",
          "lastStep": true,
          "closeAll": true
        }
      ]
    }
  }
}