window.meta = {
  "part1": {
    "title": "Background Information",
    "questions": {
      "ename": {
        "desc": "Your English name: (can be a made-up name)"
      },
      "gender": {
        "desc": "Gender",
        "option": [
          "Female",
          "Male"
        ]
      },
      "age": {
        "desc": "Age",
        "option": [
          "20-30",
          "31-40",
          "41-50"
        ]
      },
      "job": {
        "desc": "Job/major: working as a/an: (fill 'None' if not applicable)"
      },
      "studyMajor": {
        "desc": "You are studying (major): (fill 'None' if not applicable)"
      },
      "lang": {
        "desc": "Mother language/Native language",
        "option": [
          "Cantonese",
          "English"
        ]
      },
      "from": {
        "desc": "You are from:",
        "option": [
          "Hong Kong",
          "Macau"
        ]
      },
      "yrsLearn": {
        "desc": "How many years have you been learning English:",
        "type": "number"
      },
      "yrsStayEngCountry": {
        "desc": "How many months did you ever live in an English-speaking country?(Put a number, can be 0)"
      }
    }
  },
  "part2": {
    "title": "Comprehension Test",
    "note": "Note: 請根據文章上下文補充完整被部分刪除的生字。所有被刪除的字母是按以下原則刪掉：被刪掉的字母數量是原本單詞的一半或一半多一個字母。如果原本生字數量是雙數(i.e. nice- 4個字)，那麼被刪除的字母數量就是原本的一半(ni_ -被刪掉的是2個字母)；如果原本生字數量是單數(i.e. big-3個字母), 被刪除的字母數可能是原本的一半多一個(b_ -被刪掉的字母是2個)或者少一個(bi_-被刪掉的字母是1個)。",
    "fillblanks": [
      {
        "title": "My Ambition",
        "paragraph": "My one ambition in life had always been to become a writer. So, in Septe{q1} 1970, I qu{q2} the comp{q3}. I ha{q4} been work{q5} at fo{q6} eight yea{q7} and deci{q8} to bec{q9} one. Alth{q10} I succe{q11} in produ{q12} one lo{q13} novel an{q14} several sho{q15} stories du{q16} the fir{q17} nine mon{q18}, every publi{q19}  company I sho{q20} them t{q21} refused t{q22} print th{q23}. To ma{q24} matters wor{q25}, the mon{q26} I ha{q27} saved dur{q28} my yea{q29} at th{q30} company was beginning to run out."
      },
      {
        "title": "Health",
        "paragraph": "According to the National institutes of Health, the average adult sleeps less than seven hours per night. In today’s fast-paced soci{q1}. Six or sev{q2} hours o{q3} sleep m{q4} sound pre{q5} good. I{q6} reality, tho{q7}, it’s a rec{q8}       for chro{q9} sleep depriv{q10}. There i{q11}  a b{q12} difference betw{q13} the amo{q14} of sle{q15} you c{q16} get b{q17} on an{q18} the amo{q19}  you ne{q20} to func{q21} optimally. Ju{q22}  because you’re ab{q23} to ope{q24} on sev{q25}  hours o{q26} sleep doesn’t me{q27}  you wouldn’t fe{q28} a l{q29}  better an{q30} get more done if you spent an extra hour or two in bed."
      },
      {
        "title": "Titanic Sank",
        "paragraph": "More than 2.000 people were aboard when the Titanic sank. Some o{q1}    the wealt{q2} people i{q3}  the wo{q4}     were trav{q5} in th{q6} ship’s luxury cab{q71}. Hundreds o{q8} people we{q91} also o{q10} their w{q11} to Ame{q12}    to immig{q13}, and we{q14} hoping f{q15} a bet{q16} life. B{q17} as a{q18} these passe{q19} rushed tow{q20} lifeboats, ei{q21} musicians rem{q22} calm. Le{q23} by bandleader Wallace Henry Haartley, th{q24} played mu{q25} in a{q26} attempt t{q27} stop t{q28} panic. Al{q29} eight m{q30} died when the ship sank."
      },
      {
        "title": "Stress",
        "paragraph": "Stress in itself is not necessarily harmful. We ne{q1} goals a{q2} challenges i{q3}  life, o{q4} we g{q5}  bored. So{q6} people c{q7}  tolerate a{q8} sorts o{q9} major li{q10} changes wit{q11} feeling pres{12}, while oth{q13} find i{q14} difficult t{q15} cope wh{q16} life ge{q17} stressful. How{q18}, we d{q19} know th{q20} too mu{q21}is dama{q22}. Stress i{q23} a we{q24} - known trigger for depression and can also affect your physical health- so it is important to identify the stress factors in your life and do everything you can to minimise them.\n"
      }
    ]
  },
  "part3": {
    "title": "Email Writing",
    "stepQuestions": {
      "roomate": [
        {
          "name": "exchange-roomate",
          "reading": "你有一個在大學認識的好朋友，他/她是一位交換生（只會說英文），你們曾經是一年的室友，但是你們已經有兩年沒有見面了。最近你準備到這個朋友的國家旅行，想知道是否能在他/她家借宿幾天，以及讓他/她給你推薦一些在當地的美景美食等等。",
          "postRead": "Please write an email to your friend according to the situation described above. (you may make up a name for your friend)",
          "next": "exchange-roomate-reply"
        },
        {
          "name": "exchange-roomate-reply",
          "hide": true,
          "preRead": "The following is the reply you received from your friend regarding your request. Please write an email back to your friend.",
          "reading": "Reply from your friend\n\nHey friend,\nIt’s been so long! Nice to hear from you! I can’t believe that you’re coming to visit. I’m so excited! Of course I’ll be happy to host you. You can stay with me while you are here, and I’ll definitely bring you to some great food and places. Just let me know when you’re coming so I can make some plans 😊.\nCheers,\n",
          "lastStep": true
        }
      ],
      "apply-leave": [
        {
          "name": "apply-leave",
          "reading": "你在一家外企小公司剛工作了半年。由於私人原因你想要向公司請十天帶薪假（年假），但通常工作满一年才能申请年假。你的直屬上司告訴你這種情況他沒有權利批假，你需要直接向公司副總裁(C. Smith)申請。由於剛到公司半年，你和這位副總還沒有見過面，於是你決定通過郵件向副總提出申請十天休假。",
          "postRead": "Please write an email to your boss C. Smith according to the situation described above.",
          "next": "apply-leave-reply"
        },
        {
          "name": "apply-leave-reply",
          "hide": true,
          "reading": "Reply from your boss\nDear XX (your name),\nI am sorry for the disappointing news, but please understand that you are not yet entitled to take the annual leave as you have not met the company’s requirement – all employees must work for the company for at least a year to take the annual time off. Should you have other question, please refer to the company policy.\nSincerely,\nC. Smith\nVice President \n",
          "preRead": "The following is the reply you received from your boss regarding your request. Please write an email back to your boss.",
          "lastStep": true,
          "closeAll": true
        }
      ]
    }
  }
}

