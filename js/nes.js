window.meta = {
  "part1": {
    "title": "Background Information",
    "questions": {
      "ename": {
        "desc": "Your English name: (can be a made-up name)"
      },
      "gender": {
        "desc": "Gender",
        "option": [
          "Female",
          "Male"
        ]
      },
      "age": {
        "desc": "Age",
        "option": [
          "20-30",
          "31-40",
          "41-50"
        ]
      },
      "job": {
        "desc": "Job/major: working as a/an: (fill 'None' if not applicable)"
      },
      "studyMajor": {
        "desc": "You are studying (major): (fill 'None' if not applicable)"
      },
      "lang": {
        "desc": "Mother language/Native language",
        "option": [
          "Cantonese",
          "English"
        ]
      },
      "from": {
        "desc": "You are from:",
        "option": [
          "United States",
          "Australia",
          "Other"
        ]
      }
    }
  },
  "part3": {
    "title": "Email Writing",
    "stepQuestions": {
      "roomate": [
        {
          "name": "exchange-roomate",
          "reading": "You have a friend whom you knew from university, who was an exchange student. You have become good friends since you were roommates for a year while he/she was studying here. You haven’t seen each other for two years, and now you are planning a trip to his/her country. You want to know whether you can stay with him/her for a few days during your visit. You also ask his/her suggestions on local attractions and food.",
          "postRead": "Please write an email to your friend according to the situation described above. (you may make up a name for your friend)",
          "next": "exchange-roomate-reply"
        },
        {
          "name": "exchange-roomate-reply",
          "hide": true,
          "preRead": "The following are the replies you received from your friend regarding your requests. Please write an email back to them respectively.",
          "reading": "Hey friend,\nIt’s been so long! Nice to hear from you! I can’t believe that you’re coming to visit. I’m so excited! Of course I’ll be happy to host you. You can stay with me while you are here, and I’ll definitely bring you to some great food and places. Just let me know when you’re coming so I can make some plans .\nCheers,\nB\n",
          "lastStep": true
        }
      ],
      "apply-leave": [
        {
          "name": "apply-leave",
          "reading": "You have worked in a small company for about six months, and you want to ask for ten days of paid time off for personal reasons, although time off is normally granted only after a year of employment. Your direct supervisor told you that only the Vice President C.Smith can make this decision. You have not met the Vice President yet, so you decided to write to him/her to request the ten-day leave.",
          "postRead": "Please write an email to your boss C. Smith according to the situation described above.",
          "next": "apply-leave-reply"
        },
        {
          "name": "apply-leave-reply",
          "hide": true,
          "reading": "Dear XX (your name),\nI am sorry for the disappointing news, but please understand that you are not yet entitled to take the annual leave as you have not met the company’s requirement – all employees must work for the company for at least a year to take the annual time off. Should you have other question, please refer to the company policy.\nSincerely,\nC.Smith\nVice President\n",
          "preRead": "The following are the replies you received from your friend and your boss regarding your requests. Please write an email back to them respectively.",
          "lastStep": true,
          "closeAll": true
        }
      ]
    }
  }
}